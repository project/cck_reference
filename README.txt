CCK Reference

Add references in text areas via an input filter to citations stored in CCK 
multigroup fields. The input filter must be enabled for this module to work.
Citations are displayed as a hover/tool-tip when the mouse is placed over
the reference. The multigroup should contain a text field for the citation 
ID and a text field for the citation text itself.

References are added to text areas via a tag of the format [ref <ID>], where 
<ID> is the ID associated with a citation in the CCK multigroup. Multiple
references can be seperated with a |, eg: [ref 1|2|3]. The tag(s) is(are)
replaced with the ID text in the selected format. Multiple references are
displayed separated by commas, eg [1,2,3]. The IDs may be any  text string, 
so you can use any ID/numbering/labelling system you like. The IDs should be 
unique for a node (if they're not then the first one found will be used when 
searching for an associated citation).

To enable for a node type go to the node types settings page and select the
fields for the ID and citation text. Only text fields in CCK multigroups are 
shown, and only if the multigroup contains at least two text fields).

In the node type settings page you can also specify several formatting options
for references in nodes of that type, such as whether to display the references 
in superscript, a prefix and suffix character or string (eg to create 
references like [a]), and the text to display in the hover tip if the citation
corresponding to an ID can't be found.
